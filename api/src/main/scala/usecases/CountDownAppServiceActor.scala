package usecases

import akka.actor.{Actor, Props}

import scala.language.postfixOps

object CountDownAppServiceActor {
  def props: Props = {
    Props(classOf[CountDownAppServiceActor])
  }

  def props(value: String): Props = {
    Props(classOf[CountDownAppServiceActor], value)
  }
}

case class StartCountDownMessage(from: Int)

class CountDownAppServiceActor extends Actor {
  override def receive: PartialFunction[Any, Unit] = {
    case StartCountDownMessage(from) =>
      sender() ! (0 to from).reverse.toList
  }
}