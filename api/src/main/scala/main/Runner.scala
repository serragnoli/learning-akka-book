package main

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCodes.OK
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.pattern.ask
import akka.stream.ActorMaterializer
import akka.util.Timeout
import usecases.{CountDownAppServiceActor, StartCountDownMessage}

import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.Properties.envOrElse
import scala.util.{Failure, Random, Success}

object Runner {
  def main(args: Array[String]) {
    implicit val system = ActorSystem("my-system")
    implicit val materialized = ActorMaterializer()
    implicit val executionContext = system.dispatcher
    implicit val timeout = Timeout(5 seconds)

    val countDownService = system.actorOf(CountDownAppServiceActor props, "CountDownAppServiceActor")

    val route = path("count") {
      get {
        parameters("from") { (from) =>
          complete {
            for {
              r <- (countDownService ? StartCountDownMessage(from.toInt)).mapTo[List[Int]]
            } yield {
              HttpResponse(status = OK, entity = s"OK - Container: ${envOrElse("HOSTNAME", "NO HOSTNAME VAR")} - $r")
            }
          }
        }
      }
    } ~
    path("health") {
      get {
        complete {
          "OK"
        }
      }
    } ~
    path("metrics") {
      get {
        complete {
          s"fabio_first_prometheus_count=${Random.nextInt(10)}"
        }
      }
    }

    Http().bindAndHandle(route, "0.0.0.0", 8080) onComplete {
      case Success(_) => println("Server online at http://localhost:8080")
      case Failure(_) => println("Binding Failure on Sync")
    }
  }
}
