package com.fabs

import java.util.Date
import javax.servlet.annotation.WebServlet

import com.vaadin.annotations.{Theme, VaadinServletConfiguration}
import com.vaadin.server.{VaadinRequest, VaadinServlet}
import com.vaadin.ui.Button.{ClickEvent, ClickListener}
import com.vaadin.ui._

@WebServlet(urlPatterns = Array("/*"), name = "MyScalaUIServlet", asyncSupported = true)
@VaadinServletConfiguration(ui = classOf[MyScalaUI], productionMode = false)
class MyScalaUIServlet extends VaadinServlet {
}

@Theme("mytheme")
class MyScalaUI extends UI {

  override def init(request: VaadinRequest): Unit = {
    val content: VerticalLayout = new VerticalLayout
    setContent(content)

    val label: Label = new Label("Hello, world!")
    content addComponent label

    // Handle user interaction
    content addComponent new Button("Click Me from Scala!",
      new ClickListener {
        override def buttonClick(event: ClickEvent): Unit =
          Notification.show("The time is " + new Date)
      })
  }
}

