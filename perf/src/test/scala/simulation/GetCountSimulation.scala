package simulation

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scenarios.GetCountCheck
import util.Environment

import scala.language.postfixOps
import scala.concurrent.duration._

class GetCountSimulation extends Simulation {
  private val httpConf = http.baseURL(Environment.baseUrl)

  val getHealthScenarios = List(
    GetCountCheck.getHealth.inject(
      atOnceUsers(100),
      rampUsersPerSec(100) to 500 during (60 seconds)
    )
  )

  setUp(getHealthScenarios)
    .protocols(httpConf)
    .maxDuration(1 minute)
    .assertions(
      global.responseTime.percentile4.lte(500)
    )
}
