package scenarios

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.request.builder.HttpRequestBuilder
import util.Environment

object GetCountCheck {
  val getHealthHttp: HttpRequestBuilder = http("get health")
    .get(Environment.baseUrl + "count?from=10")
    .check(status is 200)

  val getHealth: ScenarioBuilder = scenario("get health")
    .exec(getHealthHttp)
}
