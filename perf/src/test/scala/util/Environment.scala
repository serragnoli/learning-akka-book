package util

object Environment {
  val baseUrl: String = scala.util.Properties.envOrElse("NFT_LOAD_BALANCER_URL", "http://localhost:8080/")
  val users: String = scala.util.Properties.envOrElse("numberOfUsers", "600")
  val maxResponseTime: String = scala.util.Properties.envOrElse("maxResponseTime", "500")
}
